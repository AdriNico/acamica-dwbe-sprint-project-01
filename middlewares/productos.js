const productos = require('../models/productos')

function validar_indice(req, res, next){
    if(req.body.indice >= 0 && req.body.indice < productos.length){
        next()
    }else{
        return res.status(500).json({"mensaje": "invalido"})
    }
    
}
module.exports = validar_indice
